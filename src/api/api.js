import config from '../constants/configurations'
/*
 * it's really simpler to have all REST requests in one object
 * we can use axios there as a standart solution, but i don't see reason for it
 * window.fetch is quite useful
 * */
const api = {
  contacts: {
    getContacts: async (qnt = 1) => {
      const response = await fetch(`${config.userUrl}/?results=${qnt}&nat=gb`)

      return response.json()
    },
  },
}

export default api
