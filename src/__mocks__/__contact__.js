const contact = {
  gender: 'male',
  name: { title: 'Mr', first: 'Christian', last: 'Alexander' },
  location: {
    street: { number: 9796, name: 'New Street' },
    city: 'Preston',
    state: 'Powys',
    country: 'United Kingdom',
    postcode: 'B0D 8HZ',
    coordinates: { latitude: '57.3639', longitude: '-113.6453' },
    timezone: { offset: '-4:00', description: 'Atlantic Time (Canada), Caracas, La Paz' },
  },
  email: 'christian.alexander@example.com',
  login: {
    uuid: '71f00a52-569d-4ab8-96c7-c3d24522e785',
    username: 'goldenpeacock536',
    password: 'fields',
    salt: '2yexGb5D',
    md5: 'f36d4f23cf5a0b25c3f7f9348168639e',
    sha1: 'ae88667797472bcea7d1d47b3b49dd1641c6df9f',
    sha256: '918df0393aacc1e875193f972ff3a142fef42d85c7635d92b2882279094ba8d8',
  },
  dob: { date: '1970-04-06T11:50:31.909Z', age: 50 },
  registered: { date: '2014-05-06T13:34:11.021Z', age: 6 },
  phone: '0171 991 9988',
  cell: '0712-751-056',
  id: { name: 'NINO', value: 'ZR 96 67 67 J' },
  picture: {
    large: 'https://randomuser.me/api/portraits/men/13.jpg',
    medium: 'https://randomuser.me/api/portraits/med/men/13.jpg',
    thumbnail: 'https://randomuser.me/api/portraits/thumb/men/13.jpg',
  },
  nat: 'GB',
}

export default contact
