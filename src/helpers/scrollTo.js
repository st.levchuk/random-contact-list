import getCoords from './getCords'

/**
 * get element coords relative to the whole page
 * @param {HTMLElement} elem
 * @param {boolean} condition
 * return {object} with coords
 */
const scrollTo = (elem, condition = true) => {
  // scroll to card in tablet and desktop
  if (condition) {
    const { top } = getCoords(elem)

    window.scrollTo({ top: top - 58, behavior: 'smooth' })
  }
}

export default scrollTo
