// function for transfer array to obj
const arrayToObject = (arr, baseValue = '') => {
  const obj = {}

  arr.forEach(l => {
    obj[l] = baseValue
  })

  return obj
}

/**
 * Format contact data into obj structure for further using
 * @param {object} data
 * @param {array} tabs
 * return {object} contactsList
 */
const formatContact = (data, tabs) => {
  // make a-z obj as initial data
  const contactsList = arrayToObject(tabs, { qnt: 0, data: {} })
  const { results } = data

  // use for cycle for better performance in case large amount of data
  for (let i = 0, { length } = results; i < length; i += 1) {
    const contact = results[i]
    const {
      name: { last },
      login: { uuid },
    } = contact
    const firstLetter = last.charAt(0).toLowerCase()

    contactsList[firstLetter] = {
      qnt: contactsList[firstLetter].qnt + 1,
      data: {
        ...contactsList[firstLetter].data,
        [uuid]: contact,
      },
    }
  }

  return contactsList
}

export default formatContact
