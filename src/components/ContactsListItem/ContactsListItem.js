import React from 'react'
import ContactCard from '../ContactCard/ContactCard'
import styles from './ContactsListItem.scss'

const ContactsListItem = ({ contact, showCard, onContactHandler }) => {
  const {
    login: { uuid },
    name: { first, last },
  } = contact

  const onContactClose = () => onContactHandler(null)

  return (
    <li className={`${styles.contactItem} ${showCard ? styles.cardShowed : ''}`}>
      <div role="presentation" className={styles.fullName} onClick={() => onContactHandler(showCard ? null : uuid)}>
        {first}, <span>{last}</span>
      </div>
      {showCard && (
        <div className={styles.cardBox}>
          <ContactCard contact={contact} onContactClose={onContactClose} />
        </div>
      )}
    </li>
  )
}

export default ContactsListItem
