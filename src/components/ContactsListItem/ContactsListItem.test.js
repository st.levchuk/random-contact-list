import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react'
import ContactsListItem from './ContactsListItem'
import contact from '../../__mocks__/__contact__'

describe('ContactsListItem Test', () => {
  const onContactHandler = jest.fn()

  afterEach(() => {
    onContactHandler.mockClear()
  })

  test('ContactsListItem showCard = false', () => {
    const { container } = render(
      <ContactsListItem contact={contact} showCard={false} onContactHandler={onContactHandler} />
    )

    const item = screen.getByRole(/presentation/i)
    expect(item).toBeInTheDocument()
    expect(container.querySelector('.cardBox')).not.toBeInTheDocument()

    fireEvent.click(item)

    expect(onContactHandler).toHaveBeenCalledTimes(1)
    expect(onContactHandler).toHaveBeenCalledWith('71f00a52-569d-4ab8-96c7-c3d24522e785')
  })

  test('ContactsListItem showCard = true', () => {
    const { container } = render(<ContactsListItem contact={contact} showCard onContactHandler={onContactHandler} />)

    expect(container.querySelector('.cardShowed')).toBeInTheDocument()
    expect(screen.getByRole(/presentation/i)).toBeInTheDocument()
    expect(container.querySelector('.cardBox')).toBeInTheDocument()

    const item = screen.getByRole(/presentation/i)

    fireEvent.click(item)

    expect(onContactHandler).toHaveBeenCalledTimes(1)
    expect(onContactHandler).toHaveBeenCalledWith(null)
  })
})
