import React from 'react'
import styles from './ContactsControls.scss'

const ContactsControls = ({ contacts, activeTab, onTabHandler }) => {
  const keys = Object.keys(contacts)

  return (
    <nav className={styles.controls}>
      <ul className={styles.controlsList}>
        {keys.map(k => {
          const { qnt } = contacts[k]
          const isActive = activeTab === k
          const classNames = `${styles.control} ${!qnt ? styles.nonSelectable : ''} ${isActive ? styles.isActive : ''}`

          return (
            <li role="presentation" key={k} className={classNames} onClick={() => onTabHandler(k)}>
              <span className={styles.title}>{k}</span>
              <span className={styles.qnt}>{qnt}</span>
            </li>
          )
        })}
      </ul>
    </nav>
  )
}

export default ContactsControls
