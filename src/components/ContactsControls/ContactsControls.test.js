import React from 'react'
import { render, screen, fireEvent } from '@testing-library/react'
import ContactsControls from './ContactsControls'
import contacts from '../../__mocks__/__contacts__'

test('ContactsControls Test', () => {
  const onTabHandler = jest.fn()

  render(<ContactsControls contacts={contacts} activeTab="a" onTabHandler={onTabHandler} />)

  const listItems = screen.getAllByRole('presentation')

  expect(screen.getByRole('navigation')).toBeInTheDocument()
  expect(listItems[0].querySelector('.title')).toContainHTML('a')
  expect(listItems[0].querySelector('.qnt')).toContainHTML('1')
  expect(listItems[0].className).toContain('isActive')
  expect(listItems[2].className).toContain('nonSelectable')

  fireEvent.click(listItems[1])

  expect(onTabHandler).toHaveBeenCalledTimes(1)
})
