import React from 'react'
import { render, screen } from '@testing-library/react'
import Contacts from './Contacts'
import contacts from '../../__mocks__/__contacts__'

describe('Contacts Test', () => {
  test('empty contacts', () => {
    const { container } = render(<Contacts contacts={null} />)

    expect(container).toBeEmptyDOMElement()
  })
  test('full contact list', () => {
    const { container } = render(<Contacts contacts={contacts} />)

    const lists = screen.getAllByRole('list')

    expect(container).not.toBeEmptyDOMElement()
    expect(lists[0].className).toContain('controlsList')
    expect(lists[1].className).toContain('contactsList')
  })
})
