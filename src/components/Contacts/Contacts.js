import React, { useState, useRef } from 'react'

import ContactsControls from '../ContactsControls/ContactsControls'
import ContactsList from '../ContactsList/ContactsList'

import styles from './Contacts.scss'

const Contacts = ({ contacts }) => {
  if (!contacts) return null

  const [activeTab, setActiveTab] = useState('a')
  const contactsContainer = useRef(null)

  // change tab handler by setting new letter
  const onTabHandler = letter => {
    const elPosition = contactsContainer.current.offsetTop

    setActiveTab(letter)

    // scroll to top on mobile
    if (window.scrollY > elPosition) {
      window.scrollTo({ left: 0, top: elPosition - 20, behavior: 'smooth' })
    }
  }

  return (
    <div className={styles.container} ref={contactsContainer}>
      <ContactsControls contacts={contacts} activeTab={activeTab} onTabHandler={onTabHandler} />
      <ContactsList contacts={contacts} activeTab={activeTab} />
    </div>
  )
}

export default Contacts
