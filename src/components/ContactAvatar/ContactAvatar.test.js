import React from 'react'
import { render, screen } from '@testing-library/react'
import ContactAvatar from './ContactAvatar'

test('Contact Avatar Test', () => {
  render(<ContactAvatar picture={{ large: 'img.jpg', medium: 'img.jpg' }} fullName="Joe Doe" />)

  expect(screen.getByRole('img')).toBeInTheDocument()
  expect(screen.getByAltText(/.*? avatar/i)).toBeInTheDocument()
})
