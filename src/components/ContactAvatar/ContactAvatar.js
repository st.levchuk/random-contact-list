import React from 'react'
import styles from './ContactAvatar.scss'

const ContactAvatar = ({ picture, fullName }) => {
  const { medium, large } = picture

  return <img src={medium} srcSet={`${medium} 1x, ${large} 2x`} alt={`${fullName} avatar`} className={styles.avatar} />
}

export default ContactAvatar
