import React, { useRef, useEffect } from 'react'
import ContactAvatar from '../ContactAvatar/ContactAvatar'
import isInViewport from '../../helpers/isInViewport'
import scrollTo from '../../helpers/scrollTo'
import styles from './ContactCard.scss'

const ContactCard = ({ contact, onContactClose }) => {
  const card = useRef(null)

  const {
    name: { first, last },
    login: { username },
    location: {
      street: { number, name: streetName },
      city,
      state,
      postcode,
    },
    email,
    cell,
    picture,
  } = contact

  useEffect(() => {
    scrollTo(card.current, !isInViewport(card.current) && window.innerWidth > 767)
  }, [])

  return (
    <article className={styles.card} ref={card}>
      <button type="button" className={styles.closeBtn} onClick={() => onContactClose()} title="Close">
        Close
      </button>
      <div className={styles.layout}>
        <div className={styles.userName}>
          <span>Username:</span> {username}
        </div>
        <div className={styles.avatarBox}>
          <ContactAvatar picture={picture} fullName={`${first}${last}`} />
        </div>
        <address className={styles.infoBox}>
          <h2 className={styles.title}>
            <span>{last}</span>, {first}
          </h2>
          <p className={styles.row}>
            <strong>e-mail</strong>
            <a href={`mailto:${email}`} className={styles.value}>
              {email}
            </a>
          </p>
          <p className={styles.row}>
            <strong>phone</strong>
            <a href={`tel:${cell.replace(/-/g, '')}`} className={styles.value}>
              {cell}
            </a>
          </p>
          <p className={styles.row}>
            <strong>street</strong>
            <span className={styles.value}>{`${number} ${streetName}`}</span>
          </p>
          <p className={styles.row}>
            <strong>city</strong>
            <span className={styles.value}>{city}</span>
          </p>
          <p className={styles.row}>
            <strong>state</strong>
            <span className={styles.value}>{state}</span>
          </p>
          <p className={styles.row}>
            <strong>postcode</strong>
            <span className={styles.value}>{postcode}</span>
          </p>
        </address>
      </div>
    </article>
  )
}

export default ContactCard
