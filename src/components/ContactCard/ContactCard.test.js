import React from 'react'
import { render, screen, fireEvent } from '@testing-library/react'
import { renderHook } from '@testing-library/react-hooks'
import ContactCard from './ContactCard'
import contact from '../../__mocks__/__contact__'

describe('ContactCard Test', () => {
  const onContactClose = jest.fn()
  const scrollTo = jest.fn()
  const isInViewport = jest.fn(() => true)

  afterEach(() => {
    scrollTo.mockClear()
  })

  test('contact cards exist and close', () => {
    render(<ContactCard contact={contact} onContactClose={onContactClose} />)

    const button = screen.getByTitle(/close/i)

    expect(button).toBeInTheDocument()

    fireEvent.click(button)

    expect(screen.getByAltText(/avatar/i)).toBeInTheDocument()
    expect(screen.getByText(/username/i)).toBeInTheDocument()
    expect(screen.getByText(/e-mail/i)).toBeInTheDocument()
    expect(screen.getByText(/phone/i)).toBeInTheDocument()
    expect(screen.getByText(/city/i)).toBeInTheDocument()
    expect(screen.getByText(/state/i)).toBeInTheDocument()
    expect(screen.getByText(/postcode/i)).toBeInTheDocument()
    expect(screen.getByText(/street/i, { selector: 'strong' })).toBeInTheDocument()

    expect(onContactClose).toHaveBeenCalledTimes(1)
  })

  test('contact card scroll more than 768px', () => {
    const { container } = render(<ContactCard contact={contact} onContactClose={onContactClose} />)

    renderHook(() => {
      scrollTo(container, isInViewport(container))
    })

    expect(scrollTo).toHaveBeenCalledTimes(1)
    expect(isInViewport).toHaveBeenCalledTimes(1)

    expect(scrollTo).toHaveBeenCalledWith(container, true)
    expect(isInViewport).toHaveBeenCalledWith(container)
  })
})
