import React from 'react'
import styles from './Loader.scss'

const Loader = ({ height = 44, width = 44, r = 10, color = '#009688' }) => {
  const styleObj = {
    width: `${width}px`,
    height: `${height}px`,
  }

  return (
    <div className={styles.loader} style={styleObj}>
      <svg className={styles.svg} viewBox={`${width / 2} ${height / 2} ${width} ${height}`}>
        <circle className={styles.path} cx={width} cy={height} r={r} fill="none" strokeWidth="3" stroke={color} />
      </svg>
    </div>
  )
}

export default Loader
