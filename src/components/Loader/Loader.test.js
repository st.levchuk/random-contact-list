import React from 'react'
import { render } from '@testing-library/react'
import Loader from './Loader'

test('Loader Test', () => {
  const { container } = render(<Loader height={50} width={50} r={12} color="#00ff00" />)

  expect(container.querySelector('svg')).toBeInTheDocument()
  expect(container.querySelector('svg')).toContainElement(container.querySelector('circle'))
})
