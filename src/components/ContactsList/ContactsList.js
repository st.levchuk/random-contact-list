import React, { useState, useEffect } from 'react'
import ContactsListItem from '../ContactsListItem/ContactsListItem'
import styles from './ContactsList.scss'

const ContactsList = ({ contacts, activeTab }) => {
  const contactsList = Object.values(contacts[activeTab].data)

  // show this if we don't have any contacts in array during first data fetching and render
  if (!contactsList.length) {
    return <div className={styles.empty}>Seems, there aren&apos;t any contacts, please select another tab</div>
  }

  const [activeContact, setActiveContact] = useState(null)

  // reset current contact when we change active tab
  useEffect(() => setActiveContact(null), [activeTab])

  const onContactHandler = id => setActiveContact(id)

  const list = contactsList.map(contact => {
    const {
      login: { uuid },
    } = contact

    return (
      <ContactsListItem
        key={uuid}
        contact={contact}
        showCard={uuid === activeContact}
        onContactHandler={onContactHandler}
      />
    )
  })

  return (
    <div className={styles.contactsWrap}>
      <div className={styles.contactsScrollBox}>
        <ul className={styles.contactsList}>{list}</ul>
      </div>
    </div>
  )
}

export default ContactsList
