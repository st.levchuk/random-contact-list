import React from 'react'
import { render, screen } from '@testing-library/react'
import ContactsList from './ContactsList'
import contacts from '../../__mocks__/__contacts__'

describe('ContactsLists Test', () => {
  test('empty contact list', () => {
    render(<ContactsList contacts={contacts} activeTab="c" />)

    expect(screen.getByText(/please select another tab/i)).toBeInTheDocument()
  })
  test('full contact list', () => {
    render(<ContactsList contacts={contacts} activeTab="a" />)

    const list = screen.getByRole('list')
    expect(list).toBeInTheDocument()
    expect(list.className).toContain('contactsList')
  })
})
