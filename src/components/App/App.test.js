import React from 'react'
import { render, screen, act } from '@testing-library/react'
import api from '../../api/api'
import App from './App'
import randomContactsData from '../../__mocks__/__randomContactsData__'

jest.mock('../../api/api')
jest.useFakeTimers()

const errorMessage = 'Oops, something went wrong. Please, refresh page and try again...'

describe('testing App component', () => {
  afterEach(() => {
    api.contacts.getContacts.mockClear()
  })

  test('init App with success contacts fetch', async () => {
    api.contacts.getContacts.mockResolvedValue(randomContactsData)

    const { container } = render(<App />)

    expect(screen.getByText(/contact list/i)).toBeInTheDocument()

    expect(container.querySelector('svg')).toBeInTheDocument()

    act(() => jest.advanceTimersByTime(1000))

    expect(await screen.findByRole('navigation')).toBeInTheDocument()

    expect(api.contacts.getContacts).toHaveBeenCalledWith(120)
    expect(api.contacts.getContacts).toHaveBeenCalledTimes(1)
  })
  test('init App with failure contacts fetch', async () => {
    api.contacts.getContacts.mockRejectedValueOnce(new Error())

    const { container } = render(<App />)

    expect(screen.getByText(/contact list/i)).toBeInTheDocument()

    expect(container.querySelector('svg')).toBeInTheDocument()

    act(() => jest.advanceTimersByTime(1000))

    expect(await screen.findByText(errorMessage)).toBeInTheDocument()

    expect(api.contacts.getContacts).toHaveBeenCalledWith(120)
    expect(api.contacts.getContacts).toHaveBeenCalledTimes(1)
  })
})
