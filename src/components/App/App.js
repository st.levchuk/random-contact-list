import React, { useState, useEffect } from 'react'

import api from '../../api/api'
import config from '../../constants/configurations'
import formatContact from '../../helpers/formatContact'

import Loader from '../Loader/Loader'
import Contacts from '../Contacts/Contacts'

import styles from './App.scss'

const App = () => {
  const [fetching, setFetching] = useState(false)
  const [appError, setAppError] = useState(null)
  const [contacts, setContacts] = useState(null)

  // get data when component is initialized
  useEffect(() => {
    setFetching(true)

    api.contacts
      .getContacts(config.numberCards)
      .then(data => setContacts(formatContact(data, config.tabs)))
      .catch(e => setAppError(e))
      .finally(() => setFetching(false))
  }, [])

  // handling error if something went wrong
  if (appError) {
    return <div className={styles.error}>Oops, something went wrong. Please, refresh page and try again...</div>
  }

  return (
    <main className={styles.app}>
      <section>
        <h1 className={styles.title}>{config.title}</h1>
        <div className={styles.container}>{fetching ? <Loader /> : <Contacts contacts={contacts} />}</div>
      </section>
    </main>
  )
}

export default App
