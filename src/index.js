import '@babel/polyfill'
import React, { StrictMode } from 'react'
import { render } from 'react-dom'
import App from './components/App/App'

import './styles/index.scss'
import 'normalize.css/normalize.css'

render(
  <StrictMode>
    <App />
  </StrictMode>,
  document.getElementById('root')
)
