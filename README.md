### Random Contact List App
#### 1. Project Description
This app creates a list of contacts. It uses [randomuser.me](http://randomuser.me/) as an API.

Keywords: ````JavaScript, React, SCSS, webpack, html5````

###### [Live Demo](http://levdev.zzz.com.ua/contact-list/)

#### 2. Project Structure

````
|-configs
|-public
|-src
   |-__mocks__
   |-api
   |-components
   |-contstants
   |-helpers
   |-images
   |-styles
   |-index.html
   |-index.js
   |-setupTests.js
|-babelrc
|-.editorconfig
|-.eslintrc
|-.gitignore
|-.htaccess
|-.prettierrc
|-package.json
|-postcss.config.js
|-README.md
````

#### 3. Installation and running
1. Clone project ```git clone https://gitlab.com/st.levchuk/random-contact-list.git```
2. Install dependencies: ````yarn```` or ````npm install ````
3. Compiles and hot-reloads for development ````yarn start````
4. Compiles and minifies for production ````yarn build````
5. Lints and fixes files ````yarn eslint````

#### 4. Comments about technical solution
* This is a simple application so I haven't seen any reason to use store manager as Redux and routing.
* If [randomuser.me](http://randomuser.me/) could give us possibilities to get separate user data by `uuid`, I would use ReactRouter and `useParams` hook for splitting our response and increasing performance.
* For faster development I also could use `react-create-app`, but I think it's a useful skill to know how to set up a project environment from the scratch.
* I haven't included linter in a `yarn start` command. I've set up a pre-commit hook with `husky` instead.

#### 5. Next steps
If we could change or build our own API I'd make endpoints for getting:
- all contacts by alphabet e.g: ```/contacts/index```
- all contacts by some letter e.g: ```/contacts/${letter}/index```
- getting separate user data by some ``uuid`` e.g: ```/contacts/show/${uuid}```.

This improvement could help us to reduce first http requests and make our app scalable and more modular. We also could add some routing to our app:
````
/ - index view
/${letter} - all contacts view by some letter
/${letter}/${uuid} - show some contact card
````
It stands to reason that if we had our own API we'd make `add/remove/favourite contact` functionality. On that step it seems to be logical to add ``Redux`` as a store manager.

And also add search contact and filter contact functionality.
